package com.example.anecdote.data.service

import com.example.anecdote.data.response.JokesResponse
import retrofit2.http.GET

interface JokesService {
    @GET("jokes/random")
    suspend fun getRandomJoke(): JokesResponse
}