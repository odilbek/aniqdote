package com.example.anecdote.data.remote

import com.example.anecdote.data.response.JokesResponse
import com.example.anecdote.data.service.JokesService
import javax.inject.Inject

class JokesRepository @Inject constructor(private val jokesService: JokesService) {
    suspend fun getRandomJoke() : JokesResponse{
        return jokesService.getRandomJoke()
    }

}