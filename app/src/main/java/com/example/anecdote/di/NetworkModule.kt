package com.example.anecdote.di

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.example.anecdote.data.remote.JokesRepository
import com.example.anecdote.data.service.JokesService
import com.example.anecdote.domain.usecase.jokes.JokesUseCase
import com.google.android.datatransport.runtime.dagger.Module
import com.google.android.datatransport.runtime.dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun providesOkhttp(
        @ApplicationContext context: Context
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addNetworkInterceptor(ChuckerInterceptor(context))
            .build()
    }

    @Provides
    @Singleton
    fun providesRetrofit(
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder().baseUrl("https://api.chucknorris.io/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun provideJokesService(
        retrofit: Retrofit
    ): JokesService {
        return retrofit.create(JokesService::class.java)
    }

    @Provides
    @Singleton
    fun providesJokesRepository(
        jokesService: JokesService
    ): JokesRepository {
        return JokesRepository(jokesService)
    }

    @Provides
    @Singleton
    fun providesJokesUseCase(
        jokesRepository: JokesRepository
    ): JokesUseCase {
        return JokesUseCase(jokesRepository)
    }

}
