package com.example.anecdote.domain.usecase.jokes

import com.example.anecdote.data.remote.JokesRepository
import com.example.anecdote.data.response.JokesResponse
import javax.inject.Inject

class JokesUseCase @Inject constructor(private val jokesRepository: JokesRepository) {
    suspend fun getRandomJoke() : JokesResponse{
        return jokesRepository.getRandomJoke()
    }
}