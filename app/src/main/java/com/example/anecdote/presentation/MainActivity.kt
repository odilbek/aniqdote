package com.example.anecdote.presentation

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.anecdote.R
import com.example.anecdote.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val viewModel: MainActivityViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        getRandomJoke()
        binding.btRefresh.setOnClickListener {
            getRandomJoke()
        }

    }

    fun getRandomJoke() {
        viewModel.getRandomJokes()
        viewModel.randomJoke.observe(this){
            binding.joke.text = it.value
        }
    }
}