package com.example.anecdote.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.anecdote.data.response.JokesResponse
import com.example.anecdote.domain.usecase.jokes.JokesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(private val useCase: JokesUseCase) : ViewModel() {


    val randomJoke = MutableLiveData<JokesResponse>()

    fun getRandomJokes() {
        viewModelScope.launch {
            val result = useCase.getRandomJoke()
            randomJoke.postValue(result)
        }

    }
}